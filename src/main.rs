mod libs;
mod rng;
mod systems;

use tracing::{event, Level};

use crate::{systems::mothershipv5, libs::arg_parse::Arguments};

/// Main function that runs the program
fn main() {
    let init_args = <Arguments as clap::Parser>::parse();
    libs::initalize::init(init_args);
    event!(Level::INFO, "Starting SkunkTel Roleplay Character Generator");
    let rand_char = mothershipv5::character::rng_generation::rng_generation();
    //print!("MothershipCharacter:\n{:#?}\n", &randchar);
    mothershipv5::file::export::export_character(&rand_char);
    event!(Level::INFO,"Rolling 3d6: {}", rng::dice_roll::dice_roll(3, 6, 0));
    event!(Level::INFO,"Rolling 1d20: {}", rng::dice_roll::dice_roll(1, 20, 0));
    event!(Level::INFO,"Rolling 1d20+5: {}", rng::dice_roll::dice_roll(1, 20, 5));
    event!(Level::INFO,"Rolling 1d20-5: {}", rng::dice_roll::dice_roll(1, 20, -5));
}
