use nanorand::{Rng, WyRand};
use tracing::debug;

/// Basic range rolling method
/// 
/// (Note: This is not cryptographically secure)
//pub fn range_roll(min: i64 , max: i64) -> i64 {
    //let mut rng: WyRand = WyRand::new();
    //let roll: i64 = rng.generate_range(min..=max);
    //return roll;
//}

/// Basic dice rolling method
/// 
/// 
/// (Note: This is not cryptographically secure)
pub fn dice_roll(rolls: i64, sides: i64, bonus: i64) -> i64 {
    let mut roll: i64 = 0;
    let mut rng = WyRand::new();
    for i in 0..rolls {
        roll += rng.generate_range(1..=sides) + bonus;
        debug!("[{0}]rolled: {1}", i, roll)
    }
    roll
}
