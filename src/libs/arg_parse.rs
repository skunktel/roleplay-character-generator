use clap::Parser;

/// 
#[derive(Parser, Debug)]
#[command(author, about, version, long_about = None)]
pub struct Arguments {
    /// Activate debug mode
    #[arg(short = 'd', long = "debug")]
    pub debug: bool,

    /// Set character name
    #[arg(short = 'n', long = "name")]
    pub name: String,
}
