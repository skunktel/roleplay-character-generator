use tracing::{info, debug};
use super::arg_parse::Arguments;

/// Initialize the program
pub fn init(args: Arguments) {
    match args.debug {
        true => tracing_subscriber::fmt()
            .with_max_level(tracing::Level::DEBUG)
            .init(),
        false => tracing_subscriber::fmt()
            .with_max_level(tracing::Level::INFO)
            .init(),
    }
    info!("Logging initialized");
    debug!("args: {:?}", args);
}
