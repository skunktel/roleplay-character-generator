use std::{fs};

use crate::systems::mothershipv5::character::character_struct::MothershipV5Char;

/// Export a character to a JSON file

pub fn export_character(character: &MothershipV5Char) -> () {
    let json = serde_json::to_string_pretty(&character).unwrap();
    fs::write(format!("{}_character.json",str::replace(&character.name, " ", "_")), json).expect("Unable to write file");
}