use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct MothershipV5Char {
    pub name: String,
    pub class: String,
    pub level: u32,
    pub rank_title: String,
    pub stress: i32,
    pub resolve: i32,
    pub health: i32,
    pub stats: MothershipV5CharStats,
    pub saves: MothershipV5CharSaves,
    pub loadout: Vec<String>,
    pub skills: Vec<String>,
    pub credits: u32
}

#[derive(Serialize, Deserialize, Debug)]
pub struct MothershipV5CharStats {
    pub strength: u32,
    pub speed: u32,
    pub intellect: u32,
    pub combat: u32
}

#[derive(Serialize, Deserialize, Debug)]
pub struct MothershipV5CharSaves {
    pub sanity: u32,
    pub fear: u32,
    pub body: u32,
    pub armor: u32
}

impl MothershipV5CharStats {
    pub fn new() -> Self {
        Self {
            strength: 0,
            speed: 0,
            intellect: 0,
            combat: 0
        }
    }
}

impl MothershipV5CharSaves {
    pub fn new() -> Self {
        Self {
            sanity: 0,
            fear: 0,
            body: 0,
            armor: 0
        }
    }
}

impl MothershipV5Char {
    pub fn new() -> Self {
        Self {
            name: String::new(),
            class: String::new(),
            level: 0,
            rank_title: String::new(),
            stress: 0,
            resolve: 0,
            health: 0,
            stats: MothershipV5CharStats::new(),
            saves: MothershipV5CharSaves::new(),
            loadout: Vec::new(),
            skills: Vec::new(),
            credits: 0
        }
    }
}

