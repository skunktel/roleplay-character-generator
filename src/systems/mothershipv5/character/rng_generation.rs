use super::character_struct::MothershipV5Char;

pub fn rng_generation() -> MothershipV5Char {
    let mut char_rng: MothershipV5Char = MothershipV5Char::new();
    char_rng.name = "Testin McTesterson".to_string();

    return char_rng;
}