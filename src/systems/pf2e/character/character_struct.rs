pub struct Pathfinder2eChar {
    pub name: String,
    pub class: String,
    pub level: u32,
    pub ancestry: String,
    pub background: String,
    pub size: String,
    pub alignment: String,
    pub deity: String,
    pub languages: Vec<String>,
    pub traits: Vec<String>,
    pub stats: Pathfinder2eCharStats,
    pub hit_points: i32,
    pub armor_class: u32,
    pub perception: u32,
    pub speed: u32,
    pub saves: Pathfinder2eCharSave,
}

pub struct Pathfinder2eCharStats {
    pub strength: u32,
    pub dexterity: u32,
    pub constitution: u32,
    pub intelligence: u32,
    pub wisdom: u32,
    pub charisma: u32,
}

pub struct Pathfinder2eCharSkills {
    pub acrobatics: u32,
    pub arcana: u32,
    pub athletics: u32,
    pub crafting: u32,
    pub deception: u32,
    pub diplomacy: u32,
    pub intimidation: u32,
    pub lore: Vec<Pathfinder2eCharLoreSkill>,
    pub medicine: u32,
    pub nature: u32,
    pub occultism: u32,
    pub performance: u32,
    pub religion: u32,
    pub society: u32,
    pub stealth: u32,
    pub survival: u32,
    pub thievery: u32,
}

pub struct Pathfinder2eCharLoreSkill {
    pub name: String,
    pub total: u32,
}

pub struct Pathfinder2eCharSave {
    pub fortitude: u32,
    pub reflex: u32,
    pub will: u32,
}

struct Pathfinder2eCharProfecentcy {
    name: String,
    rank: String,
}